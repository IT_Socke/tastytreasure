<?php
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Headers: *");
	
	# Datenbank-Verbindung (Server, Benutzername, Passwort, Datenbank)
	$db = mysqli_connect("localhost", "root", "", "cookbook");
	
	class Recipes
	{
		public $title;
		public $link;
		
		function __construct(string $title, string $link) {
			$this->title = $title;
			$this->link = $link;
		}
		
		// Methods
		function getTitle() {
			return $this->title;
		}
		function getLink() {
			return $this->link;
		}
	}


	function Skinnytaste()
	{
		# Globale Konst
        global $db;
		
		// If recipe in DB
		$sql 	= "SELECT count(*) as COUNTER
					FROM Recipe
					WHERE INSTR(link, 'skinny') > 0
					AND insertdate = CURRENT_DATE()";
		$query 	= mysqli_query($db, $sql);
		$row 	= mysqli_fetch_array($query);
		$count 	= $row['COUNTER'];
		
		// It is -> break RSS
		if($count >= 1)
			return;
		
		// Parameter
		$url 		= 'https://www.skinnytaste.com/feed';

		// Use file_get_contents to GET the URL
		$content 	= file_get_contents($url);
		
		// Skip Header
		$content 	= substr($content, strpos($content, '<item>'));
		
		// Recipes durchgehen
		while (strpos($content, '<title>') !== false){
			// Find Title
			$posStart 	= strpos($content, '<title>') + 7;
			$posEnd 	= strpos($content, '</title>', $posStart);
			$titel 		= substr($content, $posStart, $posEnd-$posStart);
			
			// Find Link
			$posStart 	= strpos($content, '<link>') + 6;
			$posEnd 	= strpos($content, '</link>', $posStart);
			$link 		= substr($content, $posStart, $posEnd-$posStart);
			
			// Find Publish Time
			$posStart 	= strpos($content, '<pubDate>') + 9;
			$posEnd 	= strpos($content, '</pubDate>', $posStart);
			$pubDate	= substr($content, $posStart, $posEnd-$posStart);
			
			// Find End of guid
			$endGuid 	= strpos($content, '<guid', $posStart);
			
			// Delete Content
			$content 	= substr($content, $endGuid);
			
			// If recipe in DB
			$sql 	= "SELECT count(*) as COUNTER
						FROM Recipe
						WHERE name = '$titel'
							AND link = '$link'
							AND publishtime = '$pubDate'";
            $query 	= mysqli_query($db, $sql);
            $row 	= mysqli_fetch_array($query);
            $count 	= $row['COUNTER'];
			
			// It is -> break RSS
			if($count >= 1)
				break;
			else
				RecipesToDbSkinnytaste($titel, $link, $pubDate);
		}
	}
	
	function RecipesToDbSkinnytaste($titel, $link, $pubDate)
	{
		// Parameter
		$hours 			= 0;
		$minutes 		= 0;
		$course 		= array();
		$region 		= array();
		$ingredients 	= array();
		
		// Use file_get_contents to GET the URL in question.
		$content 	= file_get_contents($link);
		
		// Find Hours
		if(strpos($content, 'wprm-recipe-total_time-hours') !== false){
			$posStart 	= strpos($content, 'wprm-recipe-total_time-hours') + 30;
			$posEnd 	= strpos($content, '</span>', $posStart);
			$hours 	= substr($content, $posStart, $posEnd-$posStart);
		}
		
		// Find Minutes
		if(strpos($content, 'wprm-recipe-total_time-minutes') !== false){
			$posStart 	= strpos($content, 'wprm-recipe-total_time-minutes') + 32;
			$posEnd 	= strpos($content, '</span>', $posStart);
			$minutes 	= substr($content, $posStart, $posEnd-$posStart);
		}			
		
		// Find Course
		if(strpos($content, 'wprm-recipe-course wprm-block-text-normal') !== false){
			$posStart 	= strpos($content, 'wprm-recipe-course wprm-block-text-normal') + 43;
			$posEnd 	= strpos($content, '</span>', $posStart);
			$course 	= substr($content, $posStart, $posEnd-$posStart);
			$course 	= explode(",", $course);
			
			foreach ($course as &$value) {
				if(substr($value, 0, 1) === " ")
					$value = substr($value, 1);
			}
		}
		
		// Find Region
		if(strpos($content, 'wprm-recipe-cuisine wprm-block-text-normal') !== false){
			$posStart 	= strpos($content, 'wprm-recipe-cuisine wprm-block-text-normal') + 44;
			$posEnd 	= strpos($content, '</span>', $posStart);
			$region 	= substr($content, $posStart, $posEnd-$posStart);
			$region 	= explode(",", $region);
			
			foreach ($region as &$value) {
				if(substr($value, 0, 1) === " ")
					$value = substr($value, 1);
			}
		}
		
		// Find ingredient 
		while(strpos($content, 'wprm-recipe-ingredient-name') !== false){
			$posStart 	= strpos($content, 'wprm-recipe-ingredient-name') + 29;
			$posEnd 	= strpos($content, '</span>', $posStart);
			$ingredient = substr($content, $posStart, $posEnd-$posStart);
			
			if(strpos($ingredient, 'wprm-recipe-ingredient-link') !== false){
				$posStart 	= strpos($ingredient, '>') + 1;
				$posEnd 	= strpos($ingredient, '</a>', $posStart);
				$ingredient = substr($ingredient, $posStart, $posEnd-$posStart);
			}
			
			array_push($ingredients, $ingredient);
			$content 	= substr($content, $posEnd);
		}
		
		// Save in DB
		RecipesToDb($titel, $link, $pubDate, $hours, $minutes, $course, $region, $ingredients);
	}
	
	
	function Therecipecritic()
	{
		# Globale Konst
        global $db;
		
		// If recipe in DB
		$sql 	= "SELECT count(*) as COUNTER
					FROM Recipe
					WHERE INSTR(link, 'therecipecritic') > 0
					AND insertdate = CURRENT_DATE()";
		$query 	= mysqli_query($db, $sql);
		$row 	= mysqli_fetch_array($query);
		$count 	= $row['COUNTER'];
		
		// It is -> break RSS
		if($count >= 1)
			return;
		
		// Parameter
		$url 		= 'https://therecipecritic.com/feed/';

		// Use file_get_contents to GET the URL
		$content 	= file_get_contents($url);
		
		// Skip Header
		$content 	= substr($content, strpos($content, '<item>'));
		
		// Recipes durchgehen
		while (strpos($content, '<title>') !== false){
			// Find Title
			$posStart 	= strpos($content, '<title>') + 7;
			$posEnd 	= strpos($content, '</title>', $posStart);
			$titel 		= substr($content, $posStart, $posEnd-$posStart);
			
			// Find Link
			$posStart 	= strpos($content, '<link>') + 6;
			$posEnd 	= strpos($content, '</link>', $posStart);
			$link 		= substr($content, $posStart, $posEnd-$posStart);
			
			// Find Publish Time
			$posStart 	= strpos($content, '<pubDate>') + 9;
			$posEnd 	= strpos($content, '</pubDate>', $posStart);
			$pubDate	= substr($content, $posStart, $posEnd-$posStart);
			
			// Find End of guid
			$endGuid 	= strpos($content, '<guid', $posStart);
			
			// Delete Content
			$content 	= substr($content, $endGuid);
			
			// If recipe in DB
			$sql 	= "SELECT count(*) as COUNTER
						FROM Recipe
						WHERE name = '$titel'
							AND link = '$link'
							AND publishtime = '$pubDate'";
            $query 	= mysqli_query($db, $sql);
            $row 	= mysqli_fetch_array($query);
            $count 	= $row['COUNTER'];
			
			// It is -> break RSS
			if($count >= 1)
				break;
			else
				RecipesToDbTherecipecritic($titel, $link, $pubDate);
		}
	}
	
	function RecipesToDbTherecipecritic($titel, $link, $pubDate)
	{
		// Parameter
		$hours 			= 0;
		$minutes 		= 0;
		$course 		= array();
		$region 		= array();
		$ingredients 	= array();
		
		// Use file_get_contents to GET the URL in question.
		$content 	= file_get_contents($link);
		
		// Find Hours
		if(strpos($content, 'wprm-recipe-total_time-hours') !== false){
			$posStart 	= strpos($content, 'wprm-recipe-total_time-hours') + 30;
			$posEnd 	= strpos($content, '</span>', $posStart);
			$hours 	= substr($content, $posStart, $posEnd-$posStart);
		}
		
		// Find Minutes
		if(strpos($content, 'wprm-recipe-total_time-minutes') !== false){
			$posStart 	= strpos($content, 'wprm-recipe-total_time-minutes') + 32;
			$posEnd 	= strpos($content, '</span>', $posStart);
			$minutes 	= substr($content, $posStart, $posEnd-$posStart);
		}
		
		// Find ingredient 
		while(strpos($content, 'wprm-recipe-ingredient-name') !== false){
			$posStart 	= strpos($content, 'wprm-recipe-ingredient-name') + 29;
			$posEnd 	= strpos($content, '</span>', $posStart);
			$ingredient = substr($content, $posStart, $posEnd-$posStart);
			
			if(strpos($ingredient, 'wprm-recipe-ingredient-link') !== false){
				$posStart 	= strpos($ingredient, '>') + 1;
				$posEnd 	= strpos($ingredient, '</a>', $posStart);
				$ingredient = substr($ingredient, $posStart, $posEnd-$posStart);
			}
			
			array_push($ingredients, $ingredient);
			$content 	= substr($content, $posEnd);
		}
		
		// Course durchgehen
		if(strpos($content, 'wprm-recipe-course wprm-block-text-normal') !== false){
			$posStart 	= strpos($content, 'wprm-recipe-course wprm-block-text-normal') + 43;
			$posEnd 	= strpos($content, '</span>', $posStart);
			$course 	= substr($content, $posStart, $posEnd-$posStart);
			$course 	= explode(",", $course);
			
			foreach ($course as &$value) {
				if(substr($value, 0, 1) === " ")
					$value = substr($value, 1);
			}
		}		
		
		// Find Region
		if(strpos($content, 'wprm-recipe-cuisine wprm-block-text-normal') !== false){
			$posStart 	= strpos($content, 'wprm-recipe-cuisine wprm-block-text-normal') + 44;
			$posEnd 	= strpos($content, '</span>', $posStart);
			$region 	= substr($content, $posStart, $posEnd-$posStart);
			$region 	= explode(",", $region);
			
			foreach ($region as &$value) {
				if(substr($value, 0, 1) === " ")
					$value = substr($value, 1);
			}
		}
		
		// Save in DB
		RecipesToDb($titel, $link, $pubDate, $hours, $minutes, $course, $region, $ingredients);
	}
	
	
	
	function RecipesToDb($titel, $link, $pubDate, $hours, $minutes, $course, $region, $ingredients)
	{
		// Globale Konst
        global $db;
		
		// html entity decode
		$titel 			= html_entity_decode($titel);
		$link 			= html_entity_decode($link);
		$pubDate 		= html_entity_decode($pubDate);
		$hours 			= html_entity_decode($hours);
		$minutes 		= html_entity_decode($minutes);
		
		// Recipe to DB
		$sql 	= "INSERT INTO Recipe (name, hours, mins, link, publishtime, insertdate)
					SELECT * FROM (SELECT '$titel' AS A, $hours as B, $minutes as C, '$link' as D, '$pubDate' as E, CURRENT_DATE()) AS tmp
					WHERE NOT EXISTS (
						SELECT name, link, publishtime 
						FROM Recipe 
						WHERE name = tmp.A
							AND link = tmp.D
							AND publishtime = tmp.E
					) LIMIT 1;";
		if (!mysqli_query($db, $sql)) { }		
		
		// Course to DB
		foreach ($course as $value) {
			$value 	= html_entity_decode($value);
			
			$sql1 	= "INSERT INTO Course (name)
						SELECT * FROM (SELECT '$value' as A) AS tmp
						WHERE NOT EXISTS (
							SELECT name FROM Course WHERE name = tmp.A
						) LIMIT 1;";
			$sql2 	= "INSERT INTO RecipeCourse (recipe_id, course_id)
						SELECT * FROM (
							SELECT (SELECT id from Recipe where name = '$titel' AND link = '$link' AND publishtime = '$pubDate') AS A,
								(SELECT id from Course where name = '$value') AS B) AS tmp
						WHERE NOT EXISTS (
							SELECT recipe_id, course_id
							FROM RecipeCourse 
							WHERE recipe_id = tmp.A
								AND course_id = tmp.B
						) LIMIT 1;";
			
			if (!mysqli_query($db, $sql1)) { } 
			else {
				if (!mysqli_query($db, $sql2)) { }
			}
		}
		
		// Region to DB
		foreach ($region as $value) {
			$value 	= html_entity_decode($value);
			
			$sql1 	= "INSERT INTO Region (name)
						SELECT * FROM (SELECT '$value' as A) AS tmp
						WHERE NOT EXISTS (
							SELECT name FROM Region WHERE name = tmp.A
						) LIMIT 1;";
			$sql2 	= "INSERT INTO RecipeRegion (recipe_id, region_id)
						SELECT * FROM (
							SELECT (SELECT id from Recipe where name = '$titel' AND link = '$link' AND publishtime = '$pubDate') AS A,
								(SELECT id from Region where name = '$value') AS B) AS tmp
						WHERE NOT EXISTS (
							SELECT recipe_id, region_id
							FROM RecipeRegion 
							WHERE recipe_id = tmp.A
								AND region_id = tmp.B
						) LIMIT 1;";
			
			if (!mysqli_query($db, $sql1)) { } 
			else {
				if (!mysqli_query($db, $sql2)) { }
			}
		}
		
		// Ingredients to DB
		foreach ($ingredients as $value) {
			$value 	= html_entity_decode($value);
			
			$sql1 	= "INSERT INTO Ingredient (name)
						SELECT * FROM (SELECT '$value' as A) AS tmp
						WHERE NOT EXISTS (
							SELECT name FROM Ingredient WHERE name = tmp.A
						) LIMIT 1;";
			$sql2 	= "INSERT INTO RecipeIngredient (recipe_id, ingredient_id)
						SELECT * FROM (
							SELECT (SELECT id from Recipe where name = '$titel' AND link = '$link' AND publishtime = '$pubDate') AS A,
								(SELECT id from Ingredient where name = '$value') AS B) AS tmp
						WHERE NOT EXISTS (
							SELECT recipe_id, ingredient_id
							FROM RecipeIngredient 
							WHERE recipe_id = tmp.A
								AND ingredient_id = tmp.B
						) LIMIT 1;";
			
			if (!mysqli_query($db, $sql1)) { } 
			else {
				if (!mysqli_query($db, $sql2)) { }
			}
		}
	}
	
	
	function Filter($course, $region, $time, $include, $exclude)
	{
		// Globale Konst
        global $db;
		
		// Parameter
		$courseSQL 		= "";
		$regionSQL 		= "";
		$timeSQL 		= "";
		$includeSQL 	= "";
		$incCount 		= count($include);
		$excludeSQL 	= "";
		$excludeWHERE 	= "";
		$return 		= array();
		
		// Build Course SQL WHERE
		foreach ($course as $value) {
			$courseSQL = $courseSQL . "'" . $value . "' ,";
		}
		strlen($courseSQL) > 1 ? $courseSQL = substr($courseSQL, 0, strlen($courseSQL)-2) : $courseSQL;
		
		// Build Region SQL WHERE
		foreach ($region as $value) {
			$regionSQL = $regionSQL . "'" . $value . "' ,";
		}
		strlen($regionSQL) > 1 ? $regionSQL = substr($regionSQL, 0, strlen($regionSQL)-2) : $regionSQL;
		
		// Build Time SQL WHERE
		if(count($time) > 0){
			if($time[1]==0){
				$fromMin 	= $time[0] % 60;
				$fromHour 	= intdiv($time[0], 60);
				
				$timeSQL = "AND r.hours >= $fromHour AND r.mins >= $fromMin";
			}
			else{
				$fromMin 	= $time[0] % 60;
				$toMin 		= $time[1] % 60;
				$fromHour 	= intdiv($time[0], 60);			
				$toHour 	= intdiv($time[1], 60);
				
				$timeSQL = "AND r.hours >= $fromHour AND r.mins >= $fromMin
							AND r.hours <= $toHour AND r.mins <= $toMin";
			}
		}
		
		// Build Include SQL WHERE
		foreach ($include as $value) {
			$includeSQL = $includeSQL . $value . "|";
		}
		strlen($includeSQL) > 1 ? $includeSQL = substr($includeSQL, 0, strlen($includeSQL)-1) : $includeSQL;
		
		// Build Exclude SQL WHERE
		foreach ($exclude as $value) {
			$excludeSQL = $excludeSQL . $value . "|";
		}
		strlen($excludeSQL) > 1 ? $excludeSQL = substr($excludeSQL, 0, strlen($excludeSQL)-1) : $excludeSQL;
		
		// Build SQL - Course & Region
		if(strlen($courseSQL) > 1 && strlen($regionSQL) > 1){
			$sqlSub	= "(SELECT DISTINCT r.id as id
						FROM Recipe AS r
							INNER JOIN RecipeCourse AS rc ON rc.recipe_id = r.id							
							INNER JOIN RecipeRegion AS rr ON rr.recipe_id = r.id
							INNER JOIN Course AS c ON c.id = rc.course_id
							INNER JOIN Region AS reg ON reg.id = rr.region_id
						WHERE 
							c.name IN ($courseSQL)
							AND reg.name IN ($regionSQL)
							$timeSQL
						ORDER BY r.publishtime)";
		} // Only Course
		elseif(strlen($courseSQL) > 1){
			$sqlSub	= "(SELECT DISTINCT r.id as id
						FROM Recipe AS r
							INNER JOIN RecipeCourse AS rc ON rc.recipe_id = r.id
							INNER JOIN Course AS c ON c.id = rc.course_id
						WHERE 
							c.name IN ($courseSQL)
							$timeSQL
						ORDER BY r.publishtime)";
		} // Only Region
		elseif(strlen($regionSQL) > 1){
			$sqlSub	= "(SELECT DISTINCT r.id as id
						FROM Recipe AS r							
							INNER JOIN RecipeRegion AS rr ON rr.recipe_id = r.id
							INNER JOIN Region AS reg ON reg.id = rr.region_id
						WHERE 
							reg.name IN ($regionSQL)
							$timeSQL
						ORDER BY r.publishtime)";
		}
		else{
			if(strlen($timeSQL) > 0){
				$timeSQL = substr($timeSQL, 4);
				$sqlSub	= "(SELECT DISTINCT r.id as id
							FROM Recipe AS r
							WHERE $timeSQL
							ORDER BY r.publishtime)";
			}
			else{
				$sqlSub	= "(SELECT DISTINCT r.id as id
							FROM Recipe AS r
							ORDER BY r.publishtime)";
			}
		}
		
		// Filter Include Ingredients
		if(strlen($includeSQL) > 1){
			$includeSQL	= " INNER JOIN (SELECT r.id, count(i.name) as ctii
							FROM Recipe AS r
								INNER JOIN RecipeIngredient AS ri ON ri.recipe_id = r.id
								INNER JOIN Ingredient AS i ON i.id = ri.ingredient_id
							WHERE i.name REGEXP '$includeSQL'
							GROUP BY r.id
							HAVING ctii >= $incCount
							ORDER BY r.publishtime) as SQLinc ON SQLinc.id = r.id";
		}
		
		// Filter Exclude Ingredients
		if(strlen($excludeSQL) > 1){
			$excludeSQL	= " INNER JOIN (SELECT r.id, count(i.name) as ctii
							FROM Recipe AS r
								INNER JOIN RecipeIngredient AS ri ON ri.recipe_id = r.id
								INNER JOIN Ingredient AS i ON i.id = ri.ingredient_id
							GROUP BY r.id
							ORDER BY r.publishtime) as SQLExc1 ON SQLExc1.id = r.id 
							INNER JOIN (SELECT r.id, count(i.name) as ctii
							FROM Recipe AS r
								INNER JOIN RecipeIngredient AS ri ON ri.recipe_id = r.id
								INNER JOIN Ingredient AS i ON i.id = ri.ingredient_id
							WHERE i.name NOT REGEXP '$excludeSQL'
							GROUP BY r.id
							ORDER BY r.publishtime) as SQLExc2 ON SQLExc2.id = r.id";
			$excludeWHERE = ' WHERE SQLExc1.ctii = SQLExc2.ctii';
		}
		
		// Final SQL
		$sql 	= "SELECT DISTINCT r.name as name, r.link as link
					FROM Recipe AS r
						INNER JOIN $sqlSub as SUBSEL ON SUBSEL.id = r.id
						$includeSQL
						$excludeSQL
					$excludeWHERE
					ORDER BY r.publishtime
					LIMIT 6;";
		
		$result = mysqli_query($db, $sql);
		
		if (mysqli_num_rows($result) > 0) {
			// output data of each row
			while($row = mysqli_fetch_assoc($result)) {
				$item = new Recipes($row["name"], $row["link"]);
				array_push($return, $item);
			}
		}
		
		return $return;
	}
	
	// Get Parameter
	$course 	= isset($_GET["course"]) ? explode(",",$_GET['course']) : array();
	$region 	= isset($_GET["region"]) ? explode(",",$_GET['region']) : array();
	$time 		= isset($_GET["time"]) ? explode(",",$_GET['time']) : array();
	$include 	= isset($_GET["include"]) ? explode(",",$_GET['include']) : array();
	$exclude 	= isset($_GET["exclude"]) ? explode(",",$_GET['exclude']) : array();
	
	// RSS To DB
	Skinnytaste();
	Therecipecritic();
	
	// Filter Recipes
	$result = Filter($course, $region, $time, $include, $exclude);
	
	// Return
	$return = "";
	
	// Build Return
	if(count($result)>0){
		foreach ($result as $value) {
			$return = $return . $value->getTitle() . ";" . $value->getLink() . "|";
		}
		$return = substr($return, 0, strlen($return)-1);
	}
	
	echo($return);
?> 