<?php
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Headers: *");
	
	class Recipes
	{
		public $title;
		public $link;
		public $picturelink;
		public $time;
		
		function __construct(string $title, string $link, string $picturelink, string $time) {
			$this->title 		= $title;
			$this->link 		= $link;
			$this->picturelink 	= $picturelink;
			$this->time 		= $time;
		}
		
		// Methods
		function getTitle() {
			return html_entity_decode($this->title);
		}
		function getLink() {
			return html_entity_decode($this->link);
		}
		function getPicturelink() {
			return html_entity_decode($this->picturelink);
		}
		function getTime() {
			return html_entity_decode($this->time);
		}
	}


	function Tastykitchen($course, $region, $time, $include, $exclude, $searchword)
	{
		// Parameter
		$return = array();
		$count 	= 0;
		
		// Parameter
		$url = 'https://tastykitchen.com/?searchtype=recipes&s=advanced&adv%5Bsearch_type%5D=all&adv%5Bkeywords%5D=';
		$url = $url . $searchword;
		$url = $url .'&adv%5Bexclude_keywords%5D=&view=rating&adv%5Bingredient_join%5D=any&adv%5Bingredients%5D=' . $include;
		$url = $url . '&adv%5Bexclude_ingredients%5D=' . $exclude;
		$url = $url . '&adv%5Bprep_time%5D=&adv%5Bcooking_time%5D=&adv%5Btotal_time%5D=' . $time;
		$url = $url . '&adv%5Bavg_rating%5D=&adv%5Bmember_name%5D=&adv%5Bnumber_ratings%5D=&Submit=Search';

		// Use file_get_contents to GET the URL
		$content = file_get_contents($url);
		
		// Recipes durchgehen
		while (strpos($content, 'recent-recipe') !== false && $count < 3){
			// Delete Content
			$content 	= substr($content, strpos($content, 'recent-recipe') + 13);
			
			// Find Link
			$content 	= substr($content, strpos($content, '<a'));
			$posStart 	= strpos($content, 'href="') + 6;
			$posEnd 	= strpos($content, '"', $posStart);
			$link 		= substr($content, $posStart, $posEnd-$posStart);
			
			// Find Title
			$content 	= substr($content, $posEnd);
			$posStart 	= strpos($content, '>') + 1;
			$posEnd 	= strpos($content, '</a>', $posStart);
			$titel 		= substr($content, $posStart, $posEnd-$posStart);
			
			// Find Picturelink
			$content 	 = substr($content, strpos($content, '<img'));
			$posStart 	 = strpos($content, 'src="') + 5;
			$posEnd 	 = strpos($content, '"', $posStart);	
			$picturelink = substr($content, $posStart, $posEnd-$posStart);
			
			// Find Preptime
			$content 	= substr($content, strpos($content, "itemprop='prepTime'"));
			$posStart 	= strpos($content, '>') + 1;
			$posEnd 	= strpos($content, '</time>', $posStart);
			$preptime	= substr($content, $posStart, $posEnd-$posStart);
			
			// Find Cooktime
			$content 	= substr($content, strpos($content, "itemprop='cookTime'"));
			$posStart 	= strpos($content, '>') + 1;
			$posEnd 	= strpos($content, '</time>', $posStart);
			$cooktime	= substr($content, $posStart, $posEnd-$posStart);
			
			// Save Recipe
			$item = new Recipes($titel, $link, $picturelink, (int)$preptime+(int)$cooktime . ' min');
			array_push($return, $item);
			
			$count = $count + 1;
		}
		
		return $return;
	}
	
	
	function Epicurious($course, $region, $time, $include, $exclude, $searchword, $iCount = 3)
	{
		// Parameter
		$return 	= array();
		$count 		= 0;
		$courses 	= explode(",", $course);
		$type 		= "";
		$meal 		= "";
		
		// Parameter
		$url = 'https://www.epicurious.com/search/' . $searchword;
		$url = $url .'?cuisine=' . strtolower($region);

		foreach ($courses as $value) {
			if ($value === "Soup") {
				$type = $type . "soup-stew,";
			} elseif ($value === "Salad") {
				$type = $type . "salad,";
			}
		}
		if(strlen($type) > 0 ){
			$type = substr($type, 0, (strlen($type)-1));
		}
		$url = $url .'&type=' . strtolower($type);


		foreach ($courses as $value) {
			if ($value !== "Soup" && $value !== "Salad") {
				$meal = $meal . $value . ",";
			}
		}
		if(strlen($meal) > 0 ){
			$meal = substr($meal, 0, (strlen($meal)-1));
		}
		$url = $url .'&meal=' . strtolower($meal);
		$url = $url .'&include=' . strtolower($include);
		$url = $url .'&exclude=' . strtolower($exclude);

		// Use file_get_contents to GET the URL
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		$content = curl_exec($ch);
		curl_close($ch);
		
		// Recipes durchgehen
		while (strpos($content, 'recipe-content-card') !== false && $count < $iCount){
			// Delete Content
			$content 	= substr($content, strpos($content, 'recipe-content-card') + 19);
			
			// Find Link
			$content 	= substr($content, strpos($content, '<h4'));
			$posStart 	= strpos($content, 'href="') + 6;
			$posEnd 	= strpos($content, '"', $posStart);
			$link 		= 'https://www.epicurious.com' . substr($content, $posStart, $posEnd-$posStart);
			
			// Find Title
			$content 	= substr($content, $posEnd);
			$posStart 	= strpos($content, '>') + 1;
			$posEnd 	= strpos($content, '</a>', $posStart);
			$titel 		= substr($content, $posStart, $posEnd-$posStart);
			
			// Innner Content
			$innercontent 	= file_get_contents($link);
			
			// Find Picturelink
			$innercontent 	= substr($innercontent, strpos($innercontent, '<picture'));
			$innercontent 	= substr($innercontent, strpos($innercontent, '<img'));
			$posStart 	 	= strpos($innercontent, 'srcset="') + 8;
			$posEnd 	 	= strpos($innercontent, '"', $posStart);	
			$picturelink 	= substr($innercontent, $posStart, $posEnd-$posStart);
			
			
			// Find Total time
			if(strpos($innercontent, '<dd class="total-time">') !== false)
			{
				$posStart 	= strpos($innercontent, '<dd class="total-time">') + 23;
				$posEnd 	= strpos($innercontent, '</dd>', $posStart);
				$totaltime	= substr($innercontent, $posStart, $posEnd-$posStart);
			}
			else{
				$totaltime	= "-";
			}
			
			// Save Recipe
			$item = new Recipes($titel, $link, $picturelink, $totaltime);
			array_push($return, $item);
			
			$count = $count + 1;
		}
		
		return $return;
	}
	
	// Get Parameter
	$course 	= isset($_GET["course"]) ? $_GET['course'] : '';
	$region 	= isset($_GET["region"]) ? $_GET['region'] : '';
	$time 		= isset($_GET["time"]) ? $_GET['time'] : '';
	$include 	= isset($_GET["include"]) ? $_GET['include'] : '';
	$exclude 	= isset($_GET["exclude"]) ? $_GET['exclude'] : '';
	$search 	= isset($_GET["search"]) ? $_GET['search'] : '';
	$doTastykitchen = isset($_GET["dtk"]) ? $_GET['dtk'] : '1';
	
	$result1 = []; 
	$result2 = []; 
	
	// Search for Recipe in
	if($doTastykitchen == 1){
		$result1 = Tastykitchen($course, $region, $time, $include, $exclude, $search);
		$result2 = Epicurious($course, $region, $time, $include, $exclude, $search);
	}
	else{
		$result2 = Epicurious($course, $region, $time, $include, $exclude, $search, 6);
	}
	
	// Return
	$return = "";
	
	// Build Return
	if(count($result1)>0){
		foreach ($result1 as $value) {
			$return = $return . $value->getTitle() . "|" . $value->getLink() . "|" . $value->getPicturelink() . "|" . $value->getTime() . "||";
		}
		$return = substr($return, 0, strlen($return)-1);
	}
	if(count($result2)>0){
		if(count($result1)>0){
			$return = $return . "|";
		}
		foreach ($result2 as $value) {
			$return = $return . $value->getTitle() . "|" . $value->getLink() . "|" . $value->getPicturelink() . "|" . $value->getTime() . "||";
		}
		$return = substr($return, 0, strlen($return)-1);
	}
	
	echo($return);
?> 